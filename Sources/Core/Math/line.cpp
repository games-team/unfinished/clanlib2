/*
**  ClanLib SDK
**  Copyright (c) 1997-2009 The ClanLib Team
**
**  This software is provided 'as-is', without any express or implied
**  warranty.  In no event will the authors be held liable for any damages
**  arising from the use of this software.
**
**  Permission is granted to anyone to use this software for any purpose,
**  including commercial applications, and to alter it and redistribute it
**  freely, subject to the following restrictions:
**
**  1. The origin of this software must not be misrepresented; you must not
**     claim that you wrote the original software. If you use this software
**     in a product, an acknowledgment in the product documentation would be
**     appreciated but is not required.
**  2. Altered source versions must be plainly marked as such, and must not be
**     misrepresented as being the original software.
**  3. This notice may not be removed or altered from any source distribution.
**
**  Note: Some of the libraries ClanLib may link to may have additional
**  requirements or restrictions.
**
**  File Author(s):
**
**    Mark Page
**    (if your name is missing here, please add it)
*/

#include "Core/precomp.h"
#include "API/Core/Math/vec3.h"
#include "API/Core/Math/line.h"
#include "API/Core/Math/angle.h"
#include "API/Core/Math/rect.h"

template<typename Type>
CL_Vec2<Type> CL_Line2x<Type>::get_intersection( const CL_Line2x<Type> &second, bool &intersect ) const
{
	Type denominator = (q.x - p.x) * (second.q.y - second.p.y) - (second.q.x - second.p.x) * (q.y - p.y );
	if (denominator == ((Type) 0))
	{
		intersect = false;
		return CL_Vec2<Type>();
	}
	intersect = true;

	Type numerator = (second.p.x - p.x) * (q.y - p.y) - (q.x - p.x) * (second.p.y - p.y);

	CL_Vec2<Type> result;
	result.x = (second.p.x) + ( numerator * ( second.q.x - second.p.x ) ) / denominator;
	result.y = (second.p.y) + ( numerator * ( second.q.y - second.p.y ) ) / denominator;
	return result;
}

template<typename Type>
CL_Vec3<Type> CL_Line3x<Type>::get_intersection( const CL_Line3x<Type> &second, bool &intersect, Type range ) const
{
	Type denominator = (q.x - p.x) * (second.q.y - second.p.y) - (second.q.x - second.p.x) * (q.y - p.y );
	if (denominator == ((Type) 0))
	{
		intersect = false;
		return CL_Vec3<Type>();
	}

	Type numerator = (second.p.x - p.x) * (q.y - p.y) - (q.x - p.x) * (second.p.y - p.y);

	CL_Vec3<Type> result;
	result.x = (second.p.x) + ( numerator * ( second.q.x - second.p.x ) ) / denominator;
	result.y = (second.p.y) + ( numerator * ( second.q.y - second.p.y ) ) / denominator;
	result.z = (second.p.z) + ( numerator * ( second.q.z - second.p.z ) ) / denominator;

	// Check for intersection
	Type difference = (q.x - p.x) * (second.p.z - p.z) - (q.z - p.z) * (second.p.x - p.x);
	difference += numerator * ( (second.q.z - second.p.z) * (q.x - p.x) - (q.z - p.z) * (second.q.x - second.p.x) ) / denominator;
	difference /= (q.x - p.x);

	// Ensure positive difference
	if (difference < ((Type) 0))
	{
		difference = -difference;
	}

	if (difference <= range)
	{
		intersect = true;
	}
	else
	{
		intersect = false;
	}
	return result;
}


#define CL_LINECLIP_TOP 1
#define CL_LINECLIP_RIGHT 2
#define CL_LINECLIP_BOTTOM 4
#define CL_LINECLIP_LEFT 8

#define CL_CREATE_REGION_CODE(point_x, point_y, region_code) \
	region_code = 0; \
	if (point_y < rect.top) region_code = CL_LINECLIP_TOP; \
	else if (point_y > rect.bottom) region_code = CL_LINECLIP_BOTTOM; \
	if (point_x > rect.right) region_code |= CL_LINECLIP_RIGHT; \
	else if (point_x < rect.left) region_code |= CL_LINECLIP_LEFT;

template<typename Type>
CL_Line2x<Type> &CL_Line2x<Type>::clip(const CL_Rectx<Type> &rect, bool &clipped)
{
	// Implementing the Cohen-Sutherland line clip algorithm

	int region_code_p = 0;
	int region_code_q = 0;

	CL_CREATE_REGION_CODE(p.x, p.y, region_code_p);
	CL_CREATE_REGION_CODE(q.x, q.y, region_code_q);

	while(true)
	{
		if ( ! (region_code_p | region_code_q) )	// All inside
		{
			clipped = true;
			break;
		}
		if ( region_code_p & region_code_q )	// Regions outside the rect
		{
			clipped = false;
			break;
		}

		// Calculate the line segment to clip from an outside point to an intersection with clip edge
		Type x, y;

		// At least one endpoint is outside the clip rectangle; pick it.
		int region_code_out = region_code_p ? region_code_p: region_code_q;

		// Now find the intersection point;
		// use formulas y = y0 + slope * (x - x0), x = x0 + (1/slope)* (y - y0)
		if (region_code_out & CL_LINECLIP_TOP)			// point is above the clip rectangle
		{
			x = p.x + (q.x - p.x) * (rect.top - p.y)/(q.y - p.y);
			y = rect.top;
		}
		else if (region_code_out & CL_LINECLIP_BOTTOM)	// point is below the clip rectangle
		{
			x = p.x + (q.x - p.x) * (rect.bottom - p.y)/(q.y - p.y);
			y = rect.bottom;
		}
		else if (region_code_out & CL_LINECLIP_RIGHT)	// point is to the right of clip rectangle
		{
			y = p.y + (q.y - p.y) * (rect.right - p.x)/(q.x - p.x);
			x = rect.right;
		}
		else if (region_code_out & CL_LINECLIP_LEFT)	// point is to the left of clip rectangle
		{
			y = p.y + (q.y - p.y) * (rect.left - p.x)/(q.x - p.x);
			x = rect.left;
		}

		// Move outside point to intersection point to clip and get ready for next pass.
		if (region_code_out == region_code_p)
		{
			p.x = x;
			p.y = y;
			CL_CREATE_REGION_CODE(x, y, region_code_p);
		}
		else 
		{
			q.x = x;
			q.y = y;
			CL_CREATE_REGION_CODE(x, y, region_code_q);
		}

	}

	return *this;
}

// Explicit instantiate the versions we use:

template class CL_Line2x<int>;
template class CL_Line2x<float>;
template class CL_Line2x<double>;

template class CL_Line3x<int>;
template class CL_Line3x<float>;
template class CL_Line3x<double>;
