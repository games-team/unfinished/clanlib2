<?xml version="1.0"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>GUI Framework - ClanLib SDK</title>
<link rel="stylesheet" media="screen" type="text/css" href="clanlib.css"/>
<link rel="icon" href="gfx/favicon.png" type="image/png"/>
</head>
<body>
<div id="content">
<h1><a href="."><img src="gfx/clanlib.png" alt="ClanLib SDK" /></a></h1>
<!--
<div style="float: right;">
<a href="download.html">Download</a> :
<a href="docs.html">Documentation</a> :
<a href="development.html">Development</a> :
<a href="donations.html">Donations</a> :
<a href="http://www.rtsoft.com/clanlib">Forum</a> :
<a href="contributions.html">Contributions</a>
</div>
-->
<h2>
<img src="gfx/overview.png"/>GUI Framework
</h2>

<p>ClanLib features a complete graphical user interface (GUI) toolkit.  A
GUI toolkit normally consists of a general GUI component framework, and then
some standard components generally needed by most applications.  In clanGUI,
the different user interface elements are split into components, handled by
<span class="code">CL_GUIComponent</span>, where each component covers a
specific part of the screen and performs a specific role.  The components
are arranged in a tree-like structure, which for a simple application may
look like this:</p>

<ul>
<li><span class="code">CL_Window window("Hello GUI", &amp;gui_manager)</span>
  <ul>
  <li><span class="code">CL_ListView listview(&amp;window)</span>
    <ul>
    <li><span class="code">CL_Scrollbar vertical_scroll(&amp;listview)</span></li>
    <li><span class="code">CL_Scrollbar horizontal_scroll(&amp;listview)</span></li>
    </ul>
  </li>
  <li><span class="code">CL_Button ok_button("Ok", &amp;window)</span></li>
  <li><span class="code">CL_Button cancel_button("Cancel", &amp;window)</span></li>
  </ul>
</li>
</ul>

<p>Besides <span class="code">CL_GUIComponent</span>, the other base
components of the GUI framework are:</p>

<ul>

<li><span class="code">CL_GUIManager</span>, the class running the GUI
framework.</li>

<li><span class="code">CL_GUITheme</span>, theme engine interface used by
the theme parts.</li>

<li><span class="code">CL_GUIThemePart</span>, the theming
interface used by components to apply the current theme.</li>

<li><span class="code">CL_GUIDefaultTheme</span>, the default theme engine
which uses CSS properties to describe theme parts.</li>

<li><span class="code">CL_GUIWindowManager</span>, the interface which bridges
the components to the system hosting the GUI.</li>

<li><span class="code">CL_GUIWindowManagerSystem</span>, window manager
that places the top-level components in operating system windows.</li>

<li><span class="code">CL_GUIWindowManagerTexture</span>, window manager
that places top-level components in textures.</li>

<li><span class="code">CL_GUIMessage</span>, base message class for all
messages being sent in the GUI.</li>

</ul>

<h3>The GUI Manager</h3>

<p><span class="code">CL_GUIManager</span> manages the environment in which
GUI components run.  The functions of the GUI manager can generally be split
into these groups:</p>

<ul>
<li>Handling of the GUI message queue.</li>
<li>Root of all top-level components.</li>
<li>Container for the basic GUI subsystems: window manager, resource manager, CSS
document and so on.</li>
<li>Provides a default message loop.</li>
</ul>

<p>At the heart of the GUI system is the message queue and the message pump. 
All communication between the window manager and the GUI is done by queueing
messages onto the message queue, which then are dispatched to the relevant
GUI component.  For example, if the user hits a key, this generates a
<span class="code">CL_GUIMessage_Input</span> message, which is sent to the
currently focused component.</p>

<p>There are many situations where the application will want to filter
messages, change where they are going, or simply perform other operations
when there are no message to be dealt with.  To maximize control over what
happens between messages, the application can read and dispatch the messages
itself, or it can call the default message loop via <span class="code">
CL_GUIManager::exec()</span>.</p>

<p>But enough talking, here's how you create a GUI and pump the message
queue:</p>

<pre>
// Create GUI environment:
CL_ResourceManager resources(..);
CL_GUIDefaultTheme theme;
CL_CSSDocument css_document(..);
CL_GUIWindowManagerSystem window_manager;
CL_GUIManager gui_manager;
gui_manager.set_resources(resources);
gui_manager.set_css_document(css_document);
gui_manager.set_window_manager(&window_manager);

// Create components:
CL_GUITopLevelDescription desc;
desc.set_title("ClanLib GUI");
CL_Window window(CL_Rect(0,0, 640, 480), &gui_manager, desc);
CL_PushButton button1(&window);
button1.set_geometry(CL_Rect(100, 100, 200, 120));
button1.set_text("Okay!");
button1.func_clicked().set(&on_button1_clicked, &button1);

// Pump the message queue:
CL_AcceleratorTable accelerator_table;
while (!gui_manager.get_exit_flag())
{
	CL_GUIMessage message = gui_manager.get_message();
	gui_manager.dispatch_message(message);
	if (!message.is_consumed())
		accelerator_table.process_message(message);
}

void on_button1_clicked(CL_PushButton *button)
{
	button->exit_with_code(0);
}
</pre>

<p>The message pump above does exactly the same as <span
class="code">CL_GUIManager::exec</span>, so if you do not need any
additional processing, you can just call that one.</p>

<h3>Components</h3>

<p>As mentioned in the introduction, the screen is split into components,
which are rectangular areas where each component draws itself and processes
input.  Components can be split into two types: top-level components and
child components.</p>

<p>The top-level components are constructed using a <span
class="code">CL_GUITopLevelDescription</span> class, which is passed on to
the window manager for further interpretation.  The system window manager
creates a <span class="code">CL_DisplayWindow</span> for each top-level
component, where the information in the description class is used to give
that window a title, icon, border and other styling properties.  The texture
window manager creates a new texture for the window and ignores most of the
other properties of the description.</p>

<p>Child components are constructed by only passing a parent component to
the constructor of <span class="code">CL_GUIComponent</span>.  The window
manager is not aware of child components and does not require any special
extra data to construct them.  A child component is initially created at
(0,0) with a (0,0) size, which means that you will have to explicitly set
the geometry of the component after you created it.</p>

<h3>Component Messages</h3>

<p>The GUI system communicates with the component using GUI messages.  They
are dispatched to the component, which means that <span
class="code">CL_GUIManager::dispatch_message()</span> invokes the callback
given by <span
class="code">CL_GUIComponent::func_process_message()</span>. It can receive
messages of the following types:</p>

<ul>

<li><span class="code">CL_GUIMessage_Close</span>, sent by window manager to
top-level components when the user clicks the Close button on the
window.</li>

<li><span class="code">CL_GUIMessage_FocusChange</span>, sent by the GUI
manager to the component losing keyboard focus and the component gaining
keyboard focus.</li>

<li><span class="code">CL_GUIMessage_Input</span>, sent by to the component
receiving some user input.  Input can be mouse movements, mouse clicking,
keyboard key presses, etc.</li>

<li><span class="code">CL_GUIMessage_Resize</span>, sent when a component's
geometry changes.</li>

<li><span class="code">CL_GUIMessage_Pointer</span>, sent when the mouse
enters or leaves the area covered by the component.</li>

</ul>

<p>When the GUI system needs the component to paint itself, it will invoke
<span class="code">CL_GUIComponent::func_render()</span>.  Likewise, <span
class="code">CL_GUIComponent</span> has other callbacks you can hook into to
react to certain events.</p>

<p>Example of a component that changes color if mouse is hovering above
it:</p>

<pre>
class MyComponent : public CL_GUIComponent
{
public:
	MyComponent(const CL_GUIComponent *parent)
	: CL_GUIComponent(parent), above(false)
	{
		set_type("MyComponent");
		func_render().set(this, &amp;MyComponent::on_render);
		func_process_message.set(this, &amp;MyComponent::on_process_message);
	}
	
private:
	void on_render(CL_GraphicContext &amp;gc, const CL_Rect &amp;clip_rect)
	{
		CL_Rect client(CL_Point(0,0), get_geometry().get_size());
		CL_Draw::fill_rect(
			gc,
			client,
			above ? CL_Colord::darkkhaki : CL_Colord::lightgoldenrodyellow);
	}
	
	void on_process_message(const CL_GUIMessage &amp;message)
	{
		if (message.is_type(CL_GUIMessage_Pointer::get_type())
		{
			CL_GUIMessage_Pointer msg_pointer = message;
			switch (msg_pointer.get_pointer_type())
			{
			case CL_GUIMessage_Pointer::pointer_enter:
				above = true;
				break;
			case CL_GUIMessage_Pointer::pointer_leave:
				above = false;
				break;
			}
			// Cause repaint of component:
			request_repaint();
		}
	}

	bool above;
}
</pre>

<h3>Using clanGUI Compared to clanDisplay Rendering</h3>

<p>One of the things that confuse people the most when it comes to the GUI
is how the screen is updated. In particular, if you are attempting to port
an application using clanDisplay to clanGUI, your application typically does
something along these lines:</p>

<pre>
CL_GraphicContext gc = displaywindow.get_gc();
while (!exit)
{
	update_game_logic();
	render_game(gc);
	displaywindow.flip();
	CL_KeepAlive::process();
}
</pre>

<p>A common mistake is to assume that if you want to render the GUI, you can
simply add a call to <span class="code">gui_manager.exec()</span> just
before the <span class="code">displaywindow.flip()</span> line. <i>This will
not work.</i> The <span class="code">gui_manager.exec()</span> function will not
exit until something in the GUI calls <span
class="code">CL_GUIComponent::exit_with_code()</span>, causing only the GUI to be
rendered and your game will disappear and hang.</p>

<p>The best way to fix this problem is to get rid of your own <span
class="code">while(!exit)</span> loop and create your game screen as a <span
class="code">CL_GUIComponent</span>. Since your game probably wants to constantly
repaint itself, you can use the <span
class="code">CL_GUIComponent::set_constant_repaint()</span> function to
achieve this. Also, depending on how often your game logic is to be run, you
can either place this in the render callback function or use a <span
class="code">CL_Timer</span> to call it at desired intervals.</p>

<p>Here's a simple example how how a game component might look like:</p>

<pre>
class GameWindow : public CL_Window
{
public:
	GameWindow(CL_GUIManager *manager, const CL_DisplayWindowDescription &amp;desc)
	: CL_Window(desc)
	{
		func_render().set(this, &amp;GameWindow::on_render);
		set_constant_repaint(true);
		
		button = new CL_PushButton(this);
		button->set_geometry(CL_Rect(10, 10, 150, 25));
		button->set_text("A button on top of my game!");
	}

private:	
	void on_render(CL_GraphicContext &amp;gc, const CL_Rect &amp;clip_rect)
	{
		update_game_logic();
		render_game(gc);
	}
	
	CL_PushButton *button;
};
</pre>


</div>

</body>
</html>
