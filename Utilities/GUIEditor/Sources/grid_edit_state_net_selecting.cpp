/*
**  ClanLib SDK
**  Copyright (c) 1997-2009 The ClanLib Team
**
**  This software is provided 'as-is', without any express or implied
**  warranty.  In no event will the authors be held liable for any damages
**  arising from the use of this software.
**
**  Permission is granted to anyone to use this software for any purpose,
**  including commercial applications, and to alter it and redistribute it
**  freely, subject to the following restrictions:
**
**  1. The origin of this software must not be misrepresented; you must not
**     claim that you wrote the original software. If you use this software
**     in a product, an acknowledgment in the product documentation would be
**     appreciated but is not required.
**  2. Altered source versions must be plainly marked as such, and must not be
**     misrepresented as being the original software.
**  3. This notice may not be removed or altered from any source distribution.
**
**  Note: Some of the libraries ClanLib may link to may have additional
**  requirements or restrictions.
**
**  File Author(s):
**
**    Magnus Norddahl
*/

#include "precomp.h"
#include "grid_edit_state_net_selecting.h"
#include "grid_component.h"
#include "main_window.h"
#include "selection.h"

GridEditStateNetSelecting::GridEditStateNetSelecting()
{
}

bool GridEditStateNetSelecting::on_input_pressed(const CL_InputEvent &e)
{
	if (e.id == CL_MOUSE_LEFT)
	{
		grid->main_window->get_selection()->clear();
		grid->request_repaint();
	}
	return false;
}

bool GridEditStateNetSelecting::on_input_released(const CL_InputEvent &e)
{
	return false;
}

bool GridEditStateNetSelecting::on_input_doubleclick(const CL_InputEvent &e)
{
	return false;
}

bool GridEditStateNetSelecting::on_input_pointer_moved(const CL_InputEvent &e)
{
	return false;
}
