         Name: SQL
       Status: Windows(Y), Linux(Y)
        Level: Intermediate
   Maintainer: Core developers
      Summary: Database, SQL, Sqlite

Demonstrates the clanDatabase API together with the Sqlite provider. 
Does simple Create, Retrieve, Update and Delete operations.
