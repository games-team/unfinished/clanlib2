         Name: Text
       Status: Windows(Y), Linux(Y)
        Level: Intermediate
   Maintainer: Core developers
      Summary: Text Display

Demonstrates how to use CL_SpanLayout to wordwrap lots of text, rendered with a batched CL_Font_Texture into a CL_Texture.
