
#include "precomp.h"
#include "crash_reporter.h"

#ifdef _MSC_VER
#include <tchar.h>

#pragma warning(disable: 4535) // warning C4535: calling _set_se_translator() requires /EHa

CL_String CrashReporter::reports_directory;
CL_String CrashReporter::uploader_exe;
CrashReporter::MiniDumpWriteDumpPointer CrashReporter::func_MiniDumpWriteDump = 0;
CL_Mutex CrashReporter::mutex;

CrashReporter::CrashReporter(const CL_String &new_reports_directory, const CL_String &new_uploader_executable)
{
	reports_directory = new_reports_directory;
	uploader_exe = new_uploader_executable;
	load_dbg_help();
	hook_thread();
	enforce_filter(true);
}

CrashReporter::~CrashReporter()
{
}

void CrashReporter::invoke()
{
#ifdef _DEBUG
	DebugBreak(); // Bring up the debugger if it is running
#endif
	Sleep(1000); // Give any possibly logging functionality a little time before we die
	RaiseException(EXCEPTION_NONCONTINUABLE_EXCEPTION, EXCEPTION_NONCONTINUABLE_EXCEPTION, 0, 0);
}

void CrashReporter::generate_report()
{
	__try
	{
		RaiseException(EXCEPTION_BREAKPOINT, EXCEPTION_BREAKPOINT, 0, 0);
	}
	__except(generate_report_filter(GetExceptionCode(), GetExceptionInformation()))
	{
	}
}

void CrashReporter::create_dump(DumpParams *dump_params, bool launch_uploader)
{
	SYSTEMTIME systime;
	GetLocalTime(&systime);
	HANDLE file = INVALID_HANDLE_VALUE;
	TCHAR minidump_filename[MAX_PATH];
	for (int counter = 1; counter < 1000 && file == INVALID_HANDLE_VALUE; counter++)
	{
		_stprintf_s(minidump_filename, TEXT("%s%04d-%02d-%02d %02d.%02d.%02d (%d).dmp"), reports_directory.c_str(), systime.wYear, systime.wMonth, systime.wDay, systime.wHour, systime.wMinute, systime.wSecond, counter);
		file = CreateFile(minidump_filename, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, 0);
	}

	MINIDUMP_EXCEPTION_INFORMATION info;
	info.ThreadId = dump_params->thread_id;
	info.ExceptionPointers = dump_params->exception_pointers;
	info.ClientPointers = 0;
	MINIDUMP_TYPE type = static_cast<MINIDUMP_TYPE>(
		MiniDumpWithHandleData|
		MiniDumpWithProcessThreadData|
		MiniDumpWithFullMemoryInfo|
		MiniDumpWithThreadInfo);
	func_MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), file, type, &info, 0, 0);
	CloseHandle(file);

	if (launch_uploader && !uploader_exe.empty())
	{
		STARTUPINFO startup_info;
		PROCESS_INFORMATION process_info;
		memset(&startup_info, 0, sizeof(STARTUPINFO));
		memset(&process_info, 0, sizeof(PROCESS_INFORMATION));
		startup_info.cb = sizeof(STARTUPINFO);
		if (CreateProcess(uploader_exe.c_str(), minidump_filename, 0, 0, FALSE, 0, 0, 0, &startup_info, &process_info))
		{
			CloseHandle(process_info.hThread);
			CloseHandle(process_info.hProcess);
		}
	}
}

int CrashReporter::generate_report_filter(unsigned int code, struct _EXCEPTION_POINTERS *ep)
{
	DumpParams dump_params;
	dump_params.hprocess = GetCurrentProcess();
	dump_params.hthread = GetCurrentThread();
	dump_params.thread_id = GetCurrentThreadId();
	dump_params.exception_pointers = ep;
	dump_params.exception_code = code;
	create_dump(&dump_params, false);
	return EXCEPTION_EXECUTE_HANDLER;
}

void CrashReporter::load_dbg_help()
{
	module_dbghlp = LoadLibrary(TEXT("dbghelp.dll"));
	func_MiniDumpWriteDump = reinterpret_cast<MiniDumpWriteDumpPointer>(GetProcAddress(module_dbghlp, "MiniDumpWriteDump"));
}

void CrashReporter::hook_thread()
{
	set_terminate(&CrashReporter::on_terminate);
	_set_abort_behavior(0, _CALL_REPORTFAULT|_WRITE_ABORT_MSG);
	signal(SIGABRT, &CrashReporter::on_sigabort);
	_set_se_translator(&CrashReporter::on_se_unhandled_exception);
	SetUnhandledExceptionFilter(&CrashReporter::on_win32_unhandled_exception);
}

void CrashReporter::on_terminate()
{
	invoke();
}

void CrashReporter::on_sigabort(int)
{
	invoke();
}

void CrashReporter::on_se_unhandled_exception(unsigned int exception_code, PEXCEPTION_POINTERS exception_pointers)
{
	// Ignore those bloody breakpoints!
	if (exception_code == EXCEPTION_BREAKPOINT) return;

	DumpParams dump_params;
	dump_params.hprocess = GetCurrentProcess();
	dump_params.hthread = GetCurrentThread();
	dump_params.thread_id = GetCurrentThreadId();
	dump_params.exception_pointers = exception_pointers;
	dump_params.exception_code = exception_code;

	// Ensure we only get a dump of the first thread crashing - let other threads block here.
	CL_MutexSection mutex_lock(&mutex);

	// Create dump in separate thread:
	DWORD threadId;
	HANDLE hThread = CreateThread(0, 0, &CrashReporter::create_dump_main, &dump_params, 0, &threadId);
	WaitForSingleObject(hThread, INFINITE);
	TerminateProcess(GetCurrentProcess(), 255);
}

LONG CrashReporter::on_win32_unhandled_exception(PEXCEPTION_POINTERS exception_pointers)
{
	DumpParams dump_params;
	dump_params.hprocess = GetCurrentProcess();
	dump_params.hthread = GetCurrentThread();
	dump_params.thread_id = GetCurrentThreadId();
	dump_params.exception_pointers = exception_pointers;
	dump_params.exception_code = 0;

	// Ensure we only get a dump of the first thread crashing - let other threads block here.
	CL_MutexSection mutex_lock(&mutex);

	// Create minidump in seperate thread:
	DWORD threadId;
	HANDLE hThread = CreateThread(0, 0, &CrashReporter::create_dump_main, &dump_params, 0, &threadId);
	WaitForSingleObject(hThread, INFINITE);
	TerminateProcess(GetCurrentProcess(), 255);
	return EXCEPTION_EXECUTE_HANDLER;
}

DWORD WINAPI CrashReporter::create_dump_main(LPVOID thread_parameter)
{
	create_dump(reinterpret_cast<DumpParams *>(thread_parameter), true);
	TerminateProcess(GetCurrentProcess(), 255);
	return 0;
}

bool CrashReporter::enforce_filter( bool bEnforce )
{
	DWORD ErrCode = 0;
	
	// Obtain the address of SetUnhandledExceptionFilter 

	HMODULE hLib = GetModuleHandle( _T("kernel32.dll") );
	if( hLib == NULL )
	{
		ErrCode = GetLastError();
		_ASSERTE( !_T("GetModuleHandle(kernel32.dll) failed.") );
		return false;
	}

	BYTE* pTarget = (BYTE*)GetProcAddress( hLib, "SetUnhandledExceptionFilter" );
	if( pTarget == 0 )
	{
		ErrCode = GetLastError();
		_ASSERTE( !_T("GetProcAddress(SetUnhandledExceptionFilter) failed.") );
		return false;
	}

	if( IsBadReadPtr( pTarget, sizeof(original_bytes) ) )
	{
		_ASSERTE( !_T("Target is unreadable.") );
		return false;
	}

	if( bEnforce )
	{
		// Save the original contents of SetUnhandledExceptionFilter 
		memcpy( original_bytes, pTarget, sizeof(original_bytes) );

		// Patch SetUnhandledExceptionFilter 
		if( !write_memory( pTarget, patch_bytes, sizeof(patch_bytes) ) )
			return false;
	}
	else
	{
		// Restore the original behavior of SetUnhandledExceptionFilter 
		if( !write_memory( pTarget, original_bytes, sizeof(original_bytes) ) )
			return false;
	}

	// Success 
	return true;
}


bool CrashReporter::write_memory( BYTE* pTarget, const BYTE* pSource, DWORD Size )
{
	DWORD ErrCode = 0;

	// Check parameters 

	if( pTarget == 0 )
	{
		_ASSERTE( !_T("Target address is null.") );
		return false;
	}

	if( pSource == 0 )
	{
		_ASSERTE( !_T("Source address is null.") );
		return false;
	}

	if( Size == 0 )
	{
		_ASSERTE( !_T("Source size is null.") );
		return false;
	}

	if( IsBadReadPtr( pSource, Size ) )
	{
		_ASSERTE( !_T("Source is unreadable.") );
		return false;
	}

	// Modify protection attributes of the target memory page 

	DWORD OldProtect = 0;

	if( !VirtualProtect( pTarget, Size, PAGE_EXECUTE_READWRITE, &OldProtect ) )
	{
		ErrCode = GetLastError();
		_ASSERTE( !_T("VirtualProtect() failed.") );
		return false;
	}

	// Write memory 

	memcpy( pTarget, pSource, Size );

	// Restore memory protection attributes of the target memory page 

	DWORD Temp = 0;

	if( !VirtualProtect( pTarget, Size, OldProtect, &Temp ) )
	{
		ErrCode = GetLastError();
		_ASSERTE( !_T("VirtualProtect() failed.") );
		return false;
	}

	// Success 
	return true;
}

#if defined(_M_X64)
const BYTE CrashReporter::patch_bytes[6] = { 0x48, 0x31, 0xC0, 0xC2, 0x00, 0x00 };
#elif defined(_M_IX86)
const BYTE CrashReporter::patch_bytes[5] = { 0x33, 0xC0, 0xC2, 0x04, 0x00 };
#endif

#else

CrashReporter::CrashReporter(const CL_String &reports_directory, const CL_String &uploader_executable)
{
}

#endif
