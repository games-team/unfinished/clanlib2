         Name: Basic2D
       Status: Windows(Y), Linux(Y)
        Level: Beginner
   Maintainer: Core developers
      Summary: Line, rects, surfaces, clipping, alpha

This program draws a ClanLib logo to screen, has a moving clip-area and
some partially transparent rectangles moving around.

Linux - OpenGL
  make clean
  make
  ./basic2d

Linux - GDI (software renderer)
  make clean
  make gdi
  ./basic2d


